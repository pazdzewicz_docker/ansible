#!/bin/bash

# Run SSH Agent
eval "$(ssh-agent -s)"
ssh-add "${ANSIBLE_PRIVATE_KEY_FILE}"

# Run Command
${@}