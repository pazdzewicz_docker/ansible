FROM debian:bookworm

COPY src/entrypoint.bash /usr/local/bin/entrypoint.bash

RUN chmod +x /usr/local/bin/entrypoint.bash && \
    apt -y update && \
    apt -y install ansible \
                   ansible-lint

COPY ./ansible /etc/ansible

WORKDIR /etc/ansible

ENTRYPOINT ["entrypoint.bash"]